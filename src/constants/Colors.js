export default {
  cbBlue: "#ba181b",
  subtitle: "rgb(92, 98, 110)",
  //border: "rgb(223, 225, 226)",
  positiveGreen: "rgb(11, 130, 82)",
  negativeRed: "rgb(204, 26, 46)",
  secondarySubtitle: "rgb(79, 85, 102)",
  color: "#e5383b",
  border: "#ba181b",
};
