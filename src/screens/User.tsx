import {
  AlertDialog,
  Avatar,
  Box,
  Button,
  Center,
  CloseIcon,
  Heading,
  Icon,
  Image,
  Stack,
} from "native-base";
import React, { useRef, useState } from "react";
import { Text, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import { deleteSession } from "../features/sessionSlice/sessionsSlice";
import { deleteData } from "../features/user/userSlice";
const User = (props) => {
  const dispach = useDispatch();
  const dataUser = useSelector((state) => state.userdata);
  const [isOpen, setIsOpen] = useState(false);
  console.log("dataUser", dataUser);
  const exit = () => {
    //   console.log("exit");
    //   dispach(deleteSession());
    //   dispach(deleteData());
    setIsOpen(true);
  };
  const logout = () => {
    onClose();
    console.log("out ");
    dispach(deleteSession());
    dispach(deleteData());
    props.navigation.navigate("TopTabs");
  };
  console.log(isOpen);
  const onClose = () => setIsOpen(false);
  const cancelRef = useRef(null);

  return (
    <Box>
      <Center pt={4}>
        <Image
          size={150}
          borderRadius={100}
          alt="avatar"
          source={require("../../src/assets/perfilimg.webp")}
        />
      </Center>
      <Stack p="8" space={3}>
        <Stack>
          <Heading size="md">{dataUser.nombre}</Heading>
          <Stack>
            <Text>{dataUser.correo}</Text>
          </Stack>
        </Stack>
      </Stack>
      <Stack h={40}>
        <Button
          onPress={exit}
          variant="outline"
          colorScheme="success"
          p="5"
          m="5"
        >
          <Center>
            <Icon as={Ionicons} size="20" color="blue" name="close" />
          </Center>
        </Button>
      </Stack>
      <Center>
        <AlertDialog
          leastDestructiveRef={cancelRef}
          isOpen={isOpen}
          onClose={onClose}
        >
          <AlertDialog.Content>
            <AlertDialog.CloseButton />
            <AlertDialog.Header>Cerrar sesion</AlertDialog.Header>
            <AlertDialog.Body>Salir de la cuenta?</AlertDialog.Body>
            <AlertDialog.Footer>
              <Button.Group space={2}>
                <Button
                  colorScheme="coolGray"
                  onPress={onClose}
                  ref={cancelRef}
                >
                  Cancelar
                </Button>
                <Button colorScheme="danger" onPress={logout}>
                  Salir
                </Button>
              </Button.Group>
            </AlertDialog.Footer>
          </AlertDialog.Content>
        </AlertDialog>
      </Center>
    </Box>
  );
};

export default User;
