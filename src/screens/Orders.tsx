import { Box, Center, Heading, HStack, Pressable } from "native-base";
import React, { useEffect, useState } from "react";
import { Text, View } from "react-native";
import { useSelector } from "react-redux";
import ListOrders from "../components/ListOrders";
import firebase from "../db/firebase";
const Orders = (props) => {
  const dataUser = useSelector((state) => state.userdata);
  const [user, setUser] = useState({
    id: "",
    uid: "",
    nombre: "",
  });
  const [pedidos, setPedidos] = useState([]);
  console.log("dataUser in orders", dataUser);
  useEffect(() => {
    setUser({
      id: dataUser.id,
      uid: dataUser.uid,
      nombre: dataUser.nombre,
    });
    getPedidos();
  }, [dataUser]);

  const getPedidos = async () => {
    if (dataUser.id) {
      const dbRef = firebase.db.collection("usuarios").doc(dataUser.id);
      const doc = await dbRef.get();
      const user = doc.data();
      console.log("docdata()", user);
      console.log(JSON.stringify(user.pedidos));
      const ped = user.pedidos;
      ped.map((o) => {
        //console.log("o", o.productos);
        let p = o.productos;
        p.map((pp) => {
          console.log("p", pp);
          setPedidos([pp]);
        });
      });
    } else {
    }
  };

  const detalle = () => {
    console.log("detalle");
  };
  console.log("==================", pedidos);
  console.log(pedidos.length);
  return (
    <View>
      {pedidos.length > 0 ? (
        <Box m={5}>
          {pedidos.map((p) => {
            <ListOrders key={p.id} />;
          })}
        </Box>
      ) : (
        <Box m={5}>
          <Pressable
            onPress={detalle}
            rounded="8"
            overflow="hidden"
            borderWidth="1"
            borderColor="coolGray.300"
            maxW="96"
            shadow="3"
            bg="coolGray.100"
            p="5"
          >
            <HStack>
              <Box>
                <Heading>Pedido #14633</Heading>
              </Box>
              <Center ml={10}>
                <Text>01/12/2022</Text>
              </Center>
            </HStack>
          </Pressable>
        </Box>
      )}
    </View>
  );
};

export default Orders;
