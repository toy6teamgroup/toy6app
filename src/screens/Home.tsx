import { Box, FlatList, Heading, ScrollView } from "native-base";
import React, { useEffect, useState } from "react";
import { Text, View } from "react-native";
import ListProducts from "../components/ListProducts";
import firebase from "../db/firebase";
const Home = (props) => {
  //console.log("props", props);
  const [products, setProducts] = useState([]);
  useEffect(() => {
    firebase.db
      .collection("productos")
      .where("status", "==", "1")
      .onSnapshot((querySnapshot) => {
        const pr = [];
        querySnapshot.docs.forEach((doc) => {
          pr.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        setProducts(pr);
      });
  }, []);

  // console.log("Pr", products);

  return (
    <ScrollView>
      <View>
        {products.map((product) => {
          return (
            <Box key={product.id} m={1}>
              <ListProducts itemProduct={product.data} idp={product.id} />
            </Box>
          );
        })}
      </View>
    </ScrollView>
  );
};

export default Home;
