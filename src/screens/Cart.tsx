import { Box, Button, Center, Heading, HStack, ScrollView } from "native-base";
import React, { useState, useEffect } from "react";
import { Text, View } from "react-native";
import { useSelector } from "react-redux";
import ListCart from "../components/ListCart";

const Cart = (props) => {
  const dataCart = useSelector((state) => state.cart);
  const [cart, setCart] = useState(dataCart);
  const size = cart.length;
  const [total, setTotal] = useState(0);
  useEffect(() => {
    // console.log("dataCart in cart ", dataCart);
    // console.log("cart state :", cart);
    // console.log(size);
    console.log("efecto en cart ");
    console.log("tamaño ", size);
    cart.map((dataCart) => {
      console.log("map dataCart : ", dataCart);
      let res = total + parseInt(dataCart.precio);
      setTotal(res);
    });
  }, [dataCart]);

  return (
    <ScrollView m={2}>
      {cart.length > 0 ? (
        <Box flex={1}>
          <Box flex={1}>
            {cart.map((data) => {
              return (
                <Box key={data.id}>
                  <ListCart itemCart={data} />
                </Box>
              );
            })}
          </Box>
          <Box mt={20} safeAreaTop>
            <HStack>
              <Heading position="absolute" bottom="0" px="3" py="1.5">
                Total
              </Heading>
              <Heading position="absolute" bottom="0" px="40" py="1.5">
                $ {total}
              </Heading>
            </HStack>
            <Center m={3}>
              <Button bgColor={"red.600"} w={"75%"} h={"100px"}>
                Pagar
              </Button>
            </Center>
          </Box>
        </Box>
      ) : (
        <Heading m={2}>
          <Text>No hay productos</Text>
        </Heading>
      )}
    </ScrollView>
  );
};

export default Cart;
