import React, { useState } from "react";
import {
  Box,
  Center,
  Text,
  Heading,
  VStack,
  FormControl,
  Button,
  Input,
  useToast,
  Alert,
  HStack,
  IconButton,
  CloseIcon,
  View,
} from "native-base";
import firebase from "../../db/firebase";

const Register = (props) => {
  const toast = useToast();

  let titleToast;
  let variantToast = "left-accent";
  let descriptionToast;
  let statusToast; //verde=>success//rojo=>error//azul=>info//amarillo=>warning
  let closed = true;

  const initalState = {
    nombre: "",
    correo: "",
    pass: "",
    pass1: "",
  };
  const [state, setState] = useState(initalState);

  const handleChangeText = (value, name) => {
    setState({ ...state, [name]: value });
  };

  const guardar = async () => {
    console.log("guardar ");
    console.log("state", state);
    if (
      state.nombre == "" ||
      state.correo == "" ||
      state.pass == "" ||
      state.pass1 == ""
    ) {
      console.log("en if ");
      titleToast = "Sin datos";
      statusToast = "error";
      descriptionToast = "sin datos";
    } else {
      console.log("en else");
      if (state.pass == state.pass1) {
        console.log("iguales");
        try {
          const registeruser =
            await firebase.auth.createUserWithEmailAndPassword(
              state.correo,
              state.pass
            );
          console.log("registeruser", registeruser);
          const docUsuario = await firebase.db.collection("usuarios").add({
            correo: state.correo,
            nombre: state.nombre,
            uid: registeruser.user.uid,
            status: "1",
          });
          console.log("docUsuario", docUsuario);
          titleToast = "Bienvenido";
          statusToast = "success";
          descriptionToast = "Usuario creado";
        } catch (error) {
          console.log("error", error);
          titleToast = "Error";
          statusToast = "error";
          descriptionToast = error;
        }
      } else {
        console.log("diferentes");
        titleToast = "Error";
        statusToast = "error";
        descriptionToast = "Contraseña diferente";
      }
    }
    setTimeout(() => {
      callToast();
    }, 1000);
  };

  const ToastAlert = ({
    id,
    status,
    variant,
    title,
    description,
    isClosable,
    ...rest
  }) => (
    <Center m="5">
      <Alert
        maxWidth="100%"
        alignSelf="center"
        flexDirection="row"
        status={status ? status : "info"}
        variant={variant}
        {...rest}
      >
        <VStack space={1} flexShrink={1} w="100%">
          <HStack
            flexShrink={1}
            alignItems="center"
            justifyContent="space-between"
          >
            <HStack space={2} flexShrink={1} alignItems="center">
              <Alert.Icon />
              <Text
                fontSize="md"
                fontWeight="medium"
                flexShrink={1}
                color={
                  variant === "solid"
                    ? "lightText"
                    : variant !== "outline"
                    ? "darkText"
                    : null
                }
              >
                {title}
              </Text>
            </HStack>
            {isClosable ? (
              <IconButton
                variant="unstyled"
                icon={<CloseIcon size="3" />}
                _icon={{
                  color: variant === "solid" ? "lightText" : "darkText",
                }}
                onPress={() => toast.close(id)}
              />
            ) : null}
          </HStack>
          <Text
            px="6"
            color={
              variant === "solid"
                ? "lightText"
                : variant !== "outline"
                ? "darkText"
                : null
            }
          >
            {description}
          </Text>
        </VStack>
      </Alert>
    </Center>
  );

  function callToast() {
    toast.show({
      render: ({ id }) => {
        return (
          <ToastAlert
            id={id}
            status={statusToast}
            variant={variantToast}
            title={titleToast}
            description={descriptionToast}
            isClosable={closed}
          />
        );
      },
    });
  }

  return (
    <Center w="100%">
      <Box safeArea p="2" py="8" w="90%" maxW="290">
        <Heading
          size="lg"
          fontWeight="700"
          color="coolGray.800"
          _dark={{
            color: "warmGray.50",
          }}
        ></Heading>
        <Heading
          mt="2"
          _dark={{
            color: "warmGray.200",
          }}
          color="coolGray.600"
          size="xl"
        >
          <Text fontSize="md">Registro</Text>
        </Heading>
        <VStack space={3} mt="5">
          <FormControl>
            <FormControl.Label>Nombre</FormControl.Label>
            <Input
              onChangeText={(value) => handleChangeText(value, "nombre")}
              value={state.nombre}
            />
          </FormControl>

          <FormControl>
            <FormControl.Label>Correo</FormControl.Label>
            <Input
              onChangeText={(value) => handleChangeText(value, "correo")}
              value={state.correo}
            />
          </FormControl>
          <FormControl>
            <FormControl.Label>Contraseña</FormControl.Label>
            <Input
              type="password"
              onChangeText={(value) => handleChangeText(value, "pass")}
              value={state.pass}
            />
          </FormControl>
          <FormControl>
            <FormControl.Label>Repetir contraseña</FormControl.Label>
            <Input
              type="password"
              onChangeText={(value) => handleChangeText(value, "pass1")}
              value={state.pass1}
            />
          </FormControl>

          <Button
            mt="2"
            bgColor={"rose.600"}
            onPress={guardar}
            colorScheme="indigo"
          >
            Registrarse
          </Button>
        </VStack>
      </Box>
    </Center>
  );
};

export default Register;
