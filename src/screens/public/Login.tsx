import React, { useState, useRef, useCallback } from "react";
import {
  Box,
  Center,
  Text,
  Heading,
  VStack,
  FormControl,
  Button,
  Input,
  useToast,
  Alert,
  HStack,
  IconButton,
  CloseIcon,
} from "native-base";
import firebase from "../../db/firebase";
import Home from "../Home";
import { useSelector, useDispatch } from "react-redux";

import { changeState } from "../../features/sessionSlice/sessionsSlice";
import { logedData } from "../../features/user/userSlice";
const Login = (props) => {
  const toast = useToast();
  let titleToast;
  let variantToast = "left-accent";
  let descriptionToast;
  let closed = true;
  let statusToast; //verde=>success//rojo=>error//azul=>info//amarillo=>warning
  let paso = 0;
  let usertemp = [];
  let usertempdata = [];
  const [userfireTemp, setUserfireTemp] = useState({});

  const dispach = useDispatch();
  const initalState = {
    correo: "",
    pass: "",
  };

  const [state, setState] = useState(initalState);

  const handleChangeText = (value, name) => {
    setState({ ...state, [name]: value });
  };

  const login = async () => {
    if (
      state.correo == "" ||
      state.pass == "" ||
      state.correo.length <= 0 ||
      state.pass.length <= 0
    ) {
      titleToast = "Faltan datos";
      descriptionToast = "Llene todos los campos";
      statusToast = "warning";
    } else {
      try {
        firebase.db
          .collection("usuarios")
          .where("correo", "==", state.correo)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              // console.log("doc data", doc.data());
              // console.log("doc id", doc.id);

              usertemp.push({ correo: doc.data().correo });
              usertempdata.push({
                nombre: doc.data().nombre,
                correo: doc.data().correo,
                uid: doc.data().uid,
                id: doc.id,
              });
            });
          });
      } catch (error) {
        console.log("error", error);
      }
      setTimeout(() => {
        if (usertemp.length == 0) {
          titleToast = "Error";
          descriptionToast = "Usuario no existe ";
          statusToast = "error";
        } else {
          LogFire();
        }
      }, 500);
    }
    setTimeout(() => {
      callToast();
    }, 1000);
    dispach(changeState());
  };

  const LogFire = async () => {
    try {
      const userFire = await firebase.auth.signInWithEmailAndPassword(
        state.correo,
        state.pass
      );
      // console.log("userFire", userFire);

      setUserfireTemp(userFire);
      titleToast = "Bienvenido";
      descriptionToast = `Aceddiendo...`;
      statusToast = "success";

      setTimeout(function () {
        dispach(logedData(usertempdata));
        props.navigation.navigate("Tabs");
      }, 500);
    } catch (error) {
      titleToast = "Error";
      descriptionToast = `${error}`;
      statusToast = "error";
    }
  };

  const ToastAlert = ({
    id,
    status,
    variant,
    title,
    description,
    isClosable,
    ...rest
  }) => (
    <Center m="5">
      <Alert
        maxWidth="100%"
        alignSelf="center"
        flexDirection="row"
        status={status ? status : "info"}
        variant={variant}
        {...rest}
      >
        <VStack space={1} flexShrink={1} w="100%">
          <HStack
            flexShrink={1}
            alignItems="center"
            justifyContent="space-between"
          >
            <HStack space={2} flexShrink={1} alignItems="center">
              <Alert.Icon />
              <Text
                fontSize="md"
                fontWeight="medium"
                flexShrink={1}
                color={
                  variant === "solid"
                    ? "lightText"
                    : variant !== "outline"
                    ? "darkText"
                    : null
                }
              >
                {title}
              </Text>
            </HStack>
            {isClosable ? (
              <IconButton
                variant="unstyled"
                icon={<CloseIcon size="3" />}
                _icon={{
                  color: variant === "solid" ? "lightText" : "darkText",
                }}
                onPress={() => toast.close(id)}
              />
            ) : null}
          </HStack>
          <Text
            px="6"
            color={
              variant === "solid"
                ? "lightText"
                : variant !== "outline"
                ? "darkText"
                : null
            }
          >
            {description}
          </Text>
        </VStack>
      </Alert>
    </Center>
  );
  function callToast() {
    toast.show({
      render: ({ id }) => {
        return (
          <ToastAlert
            id={id}
            status={statusToast}
            variant={variantToast}
            title={titleToast}
            description={descriptionToast}
            isClosable={closed}
          />
        );
      },
    });
  }
  return (
    <Center w="100%">
      <Box safeArea p="2" py="8" w="90%" maxW="290">
        <Heading
          size="lg"
          fontWeight="700"
          color="coolGray.800"
          _dark={{
            color: "warmGray.50",
          }}
        ></Heading>
        <Heading
          mt="2"
          _dark={{
            color: "warmGray.200",
          }}
          color="coolGray.600"
          size="xl"
        >
          <Text fontSize="md">Inicia sesion</Text>
        </Heading>
        <VStack space={3} mt="5">
          <FormControl>
            <FormControl.Label>Correo</FormControl.Label>
            <Input
              onChangeText={(value) => handleChangeText(value, "correo")}
              value={state.correo}
            />
          </FormControl>
          <FormControl>
            <FormControl.Label>Contraseña</FormControl.Label>
            <Input
              type="password"
              onChangeText={(value) => handleChangeText(value, "pass")}
              value={state.pass}
            />
          </FormControl>
          <Button mt="2" bgColor={"rose.600"} onPress={login}>
            Acceder
          </Button>
        </VStack>
      </Box>
    </Center>
  );
};

export default Login;
