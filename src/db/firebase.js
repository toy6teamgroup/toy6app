import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDKdymPY2jHozgVmeXSkxG-uoldU8m8gZ0",
  authDomain: "toysixwebapp.firebaseapp.com",
  databaseURL: "https://toysixwebapp-default-rtdb.firebaseio.com",
  projectId: "toysixwebapp",
  storageBucket: "toysixwebapp.appspot.com",
  messagingSenderId: "376169941503",
  appId: "1:376169941503:web:eb79ecf46ecb11b0e4ffca",
  measurementId: "G-QYMMKK04M0",
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
export default {
  firebase,
  db,
  auth,
  storage,
};
