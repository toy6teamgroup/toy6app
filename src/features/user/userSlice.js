import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  nombre: "",
  correo: "",
  uid: "",
  log: false,
  id: "",
};
export const userSlice = createSlice({
  name: "userdata",
  initialState: initialState,
  reducers: {
    logedData: (state, action) => {
      console.log("state", state);
      console.log("actions logged : ", action.payload);
      state.nombre = action.payload[0].nombre;
      state.correo = action.payload[0].correo;
      state.uid = action.payload[0].uid;
      state.id = action.payload[0].id;
      state.log = true;
      console.log("nuevo estado ", state);
    },
    deleteData: (state) => {
      state.nombre = "";
      state.correo = "";
      state.uid = "";
      state.id = "";
      state.log = false;
      console.log("saliendo", state);
    },
  },
});

export const { logedData, deleteData } = userSlice.actions;
export default userSlice.reducer;
