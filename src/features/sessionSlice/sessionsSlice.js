import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: false,
};

export const sessionSlice = createSlice({
  name: "session",
  initialState: initialState,
  reducers: {
    changeState: (state) => {
      console.log("estado inicial", state, " ===", true);
      //state = true;
      state.value = true;
      console.log("cambiado", state);
    },
    deleteSession: (state) => {
      console.log("estado actual", state, " Cambio", false);
      //state = false;
      state.value = false;
      console.log("saliendo...", state);
    },
  },
});

export const { changeState, deleteSession } = sessionSlice.actions;
export default sessionSlice.reducer;
