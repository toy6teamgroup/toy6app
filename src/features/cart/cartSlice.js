import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

const cartSlice = createSlice({
  name: "cart",
  initialState: initialState,
  reducers: {
    addCart: (state, action) => {
      //console.log("======================add to cart======================");
      console.log("action :", action.payload);
      //console.log("MEDIDA INICIAL : ", state.length);

      if (state.length == 0) {
        state.push(action.payload);
      } else if (state.length > 0) {
        const resultado = state.find((dato) => dato.id === action.payload.id);
        //console.log("resultado :", resultado);

        if (resultado) {
          //console.log("repetido");
          // state.forEach((st) => {
          //   console.log("for each => ", st);
          // });
        } else {
          state.push(action.payload);
        }
      }

      // console.log("state nuevo :", state);
      // console.log("MEDIDA  : ", state.length);
    },
  },
});

export const { addCart } = cartSlice.actions;
export default cartSlice.reducer;
