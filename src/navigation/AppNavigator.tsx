import React, { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Home from "../screens/Home";
import Cart from "../screens/Cart";
import Orders from "../screens/Orders";
import User from "../screens/User";
import TabBar from "../components/TabBar";
import Login from "../screens/public/Login";
import Register from "../screens/public/Register";
import { NativeBaseProvider, View, Box, Center } from "native-base";
import { useSelector } from "react-redux";

import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Tab = createBottomTabNavigator();
const TopTab = createMaterialTopTabNavigator();

function Tabs() {
  return (
    <Tab.Navigator tabBar={(props) => <TabBar {...props} />}>
      <Tab.Screen name="Catalogo" component={Home} />
      <Tab.Screen name="Carrito" component={Cart} />
      <Tab.Screen name="Pedidos" component={Orders} />
      <Tab.Screen name="Usuario" component={User} />
    </Tab.Navigator>
  );
}

function TopTabs() {
  return (
    <TopTab.Navigator>
      <Tab.Screen name="Inicio" component={Login} />
      <Tab.Screen name="Registro" component={Register} />
    </TopTab.Navigator>
  );
}
const AppNavigator = () => {
  const dataUser = useSelector((state) => state.userdata);
  const [datalog, setData] = useState(dataUser.log);
  const [inicial, setInicial] = useState("TopTabs");

  // useEffect(() => {
  //   console.log("efecto");
  //   if (dataUser.log) {
  //     setInicial("Tabs");
  //   } else if (dataUser.log === false) {
  //     setInicial("TopTabs");
  //   }
  // }, [dataUser]);

  console.log("data", dataUser.log);

  console.log("inicial", inicial);

  const Stack = createNativeStackNavigator();
  return (
    <NativeBaseProvider>
      <Box bg="#e5383b" flex={1} rounded="xl" safeArea>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName={inicial}
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen
              name="Tabs"
              options={{
                title: "Sextoys",
              }}
              component={Tabs}
            />
            <Stack.Screen
              name="TopTabs"
              options={{
                title: "Login",
              }}
              component={TopTabs}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </Box>
    </NativeBaseProvider>
  );
};

export default AppNavigator;
