import { configureStore } from "@reduxjs/toolkit";
import sessionReducer from "../features/sessionSlice/sessionsSlice";
import userSlice from "../features/user/userSlice";
import cartSlice from "../features/cart/cartSlice";
export const store = configureStore({
  reducer: {
    session: sessionReducer,
    userdata: userSlice,
    cart: cartSlice,
  },
});
