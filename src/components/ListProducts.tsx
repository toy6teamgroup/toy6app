import {
  Badge,
  Box,
  Button,
  HStack,
  Pressable,
  Spacer,
  Text,
  VStack,
  Input,
  Center,
  Image,
  Avatar,
  useToast,
} from "native-base";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import LableCat from "./LabelCat";
import { addCart } from "../features/cart/cartSlice";

const ListProducts = (props) => {
  const toast = useToast();
  //console.log("idp ", props.idp);
  //props.itemProduct
  const [itemProduct, setItemProduct] = useState({
    cantidad: props.itemProduct.cantidad,
    imagen: props.itemProduct.imagen,
    nombre: props.itemProduct.nombre,
    id: props.idp,
    precio: props.itemProduct.precio,
    status: props.itemProduct.status,
    guardado: 1,
  });
  const dispach = useDispatch();
  //console.log(itemProduct);

  let srcimg;
  if (itemProduct.imagen) {
    srcimg = itemProduct.imagen;
  } else {
    srcimg = "https://nf.aaoinfo.org/eweb/images/DEMO1/notavailable.jpg";
  }
  let size;

  //console.log("itemProduct", itemProduct);
  const Click = () => {
    //  console.log("click");
  };
  const datacart = useSelector((state) => state.cart);

  const Agregar = () => {
    toast.show({
      render: () => {
        return (
          <Box bg="emerald.400" px="2" py="1" rounded="sm" mb={5}>
            Agregado...
          </Box>
        );
      },
    });
    let rtn = dispach(addCart(itemProduct));
    //console.log("rtn", rtn);
  };

  // useEffect(() => {
  //   console.log("==========datacart============");
  //   console.log(datacart);
  //   console.log("======================");
  // }, [datacart]);
  return (
    <Box>
      <Pressable
        onPress={Click}
        rounded="8"
        overflow="hidden"
        borderWidth="1"
        borderColor="coolGray.300"
        maxW="96"
        shadow="3"
        bg="coolGray.100"
        p="5"
      >
        <Box>
          <HStack>
            <Box m={6} alignContent="center">
              <Avatar
                bg="violet.200"
                alignSelf="center"
                size="md"
                source={{
                  uri: `${srcimg}`,
                }}
              />
            </Box>

            <VStack>
              <Text
                color="coolGray.800"
                mt="3"
                fontWeight="medium"
                fontSize="xl"
              >
                {itemProduct.nombre}
              </Text>
              <Text mt="2" fontSize="md" color="coolGray.700">
                $ {itemProduct.precio}
              </Text>
            </VStack>
          </HStack>
          <Box>
            <HStack mt={2} width="100%">
              <Button
                m={2}
                bgColor={"blue.400"}
                //colorScheme="success"
                onPress={Agregar}
                flex={1}
              >
                Agregar
              </Button>
              <Button bgColor={"violet.400"} m={2} flex={1}>
                Ver mas
              </Button>
            </HStack>
          </Box>
        </Box>
      </Pressable>
    </Box>
  );
};
export default ListProducts;
