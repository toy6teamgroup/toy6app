import { Box, HStack, Text, VStack, Image } from "native-base";
import React, { useState } from "react";
const ListCart = (props) => {
  //console.log("props list cart :", props.itemCart);
  const [item, setItem] = useState({
    id: props.itemCart.id,
    nombre: props.itemCart.nombre,
    precio: props.itemCart.precio,
    imagen: props.itemCart.imagen,
  });
  return (
    <Box
      overflow="hidden"
      borderWidth="1"
      borderColor="coolGray.300"
      rounded="8"
      maxW="96"
      shadow="3"
      bg="coolGray.100"
      p="5"
    >
      <HStack>
        <Box>
          <Image
            bg="violet.200"
            alignSelf="center"
            size="md"
            source={{
              uri: `${item.imagen}`,
            }}
            alt={item.id}
          />
        </Box>
        <VStack ml={4}>
          <Text color="coolGray.800" fontWeight="medium" fontSize="xl">
            {item.nombre}
          </Text>
          <Text mt="2" fontSize="md" color="coolGray.700">
            $ {item.precio}
          </Text>
        </VStack>
      </HStack>
    </Box>
  );
};
export default ListCart;
