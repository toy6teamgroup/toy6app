import { Badge } from "native-base";
import React, { useState } from "react";

const LableCat = (props) => {
  //console.log("props", props);
  return (
    <Badge
      colorScheme="darkBlue"
      _text={{
        color: "white",
      }}
      variant="solid"
      rounded="4"
    ></Badge>
  );
};

export default LableCat;
